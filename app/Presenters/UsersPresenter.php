<?php

declare(strict_types=1);

namespace App\Presenters;


use App\Components\Forms\SignUpFormFactory;
use App\Components\Repository\UserRepository;

final class UsersPresenter extends BasePresenter
{
    private $userRepository;
    private $signUp;

    public function __construct(UserRepository $userRepository, SignUpFormFactory $signUp)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->signUp = $signUp;
    }

    public function beforeRender()
    {
        $this->isLoggedIn();
        $user = $this->userRepository->getById($this->getUser()->getId());
        $this->template->user = $user;
        $this->permissionLevel(2,$user->role);
    }
    public function renderDefault(): void
	{
        $this->template->userss = $this->userRepository->getAllNotDeleted();
	}
    public function actionCreate(){
    }
    protected function createComponentSignUpForm()
    {
        return $this->signUp->create($this->getParameter('id') ,
            function () {
                $this->flashMessage("Uživatel byl úspěšně uložen","alert-success");
                $this->redirect('Users:');
            });
    }
    public function actionDelete($id){
        $this->userRepository->setDeleted($id);
        $this->redirect('Users:');
    }
    public function actionEdit($id){
        $dealer = $this->userRepository->getById($id);
        if (!$dealer){
            $this->AlertError("Uživatel nebyl nalezen","Users:");
        }
        $this['signUpForm']->setDefaults($dealer->toArray());
    }
}
