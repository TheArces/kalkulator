<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    public function isLoggedIn(){
        if (!$this->getUser()->isLoggedIn()){
            $this->redirect('Sign:in');
        }
    }
    public function permissionLevel($levelNeeded, $usersLevel){
        if ($levelNeeded > $usersLevel){
            $this->AlertError("Nemáte dostatečné oprávnění na zobrazení této stránky", "Homepage:");
        }
    }
    public function AlertError($message,$redirect){
        $this->flashMessage($message,"alert-danger");
        $this->redirect($redirect);
    }
}
