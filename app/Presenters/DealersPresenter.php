<?php

declare(strict_types=1);

namespace App\Presenters;


use App\Components\Forms\DealerForm;
use App\Components\Repository\DealersRepository;
use App\Components\Repository\MarginRepository;
use App\Components\Repository\UserRepository;
use App\Components\Forms\MarginForm;

final class DealersPresenter extends BasePresenter
{
    private $userRepository;
    private $dealersRepository;
    private $marginRepository;
    private $dealerForm;
    private $marginForm;

    public function __construct(UserRepository $userRepository, DealersRepository $dealersRepository, MarginRepository $marginRepository, DealerForm $dealerForm, MarginForm $marginForm)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->dealersRepository = $dealersRepository;
        $this->marginRepository = $marginRepository;
        $this->dealerForm = $dealerForm;
        $this->marginForm = $marginForm;
    }

    public function beforeRender()
    {
        $this->isLoggedIn();
        $user = $this->userRepository->getById($this->getUser()->getId());
        $this->template->user = $user;
        $this->permissionLevel(1,$user->role);
    }
    public function renderDefault(): void
	{
	    $this->template->dealers = $this->dealersRepository->getAllNotDeleted();
	}
    protected function createComponentDealerForm()
    {
        return $this->dealerForm->create($this->getParameter('id'),function () {
            $this->flashMessage("Dodavatel byl úspěšně uložen","alert-success");
            $this->redirect('Dealers:');
        });
    }
    public function actionEdit($id){
        $dealer = $this->dealersRepository->getById($id);
        if (!$dealer){
            $this->AlertError("Dodavatel nebyl nalezen","Dealers:");
        }
        $this['dealerForm']->setDefaults($dealer->toArray());
    }
    public function actionCreate(){
    }
    public function actionDelete($id){
        $this->dealersRepository->setDeleted($id);
        $this->redirect("Dealers:");
    }
    public function renderDetail($id): void
    {
        $toGet = ["dealer_id = ".$id];
        $dealer = $this->dealersRepository->getById($id);
        if (!$dealer){
            $this->AlertError("Dodavatel nebyl nalezen","Dealers:");
        }
        $this->template->dealers = $dealer;
        $this->template->margins = $this->marginRepository->getAllNotDeletedByDealerId($id);
    }
    public function actionMargincreate($id){
        $this->template->id = $id;
    }
    public function actionMarginDelete($id, $redirectTo){
        $this->marginRepository->setDeleted($id);
        $this->redirect("Dealers:detail?id=".$redirectTo);
    }
    protected function createComponentMarginForm()
    {
        return $this->marginForm->create($this->getParameter('id'),function ($dealerId) {
            $this->flashMessage("Marže byla úspěšně uložena","alert-success");
            $this->redirect('Dealers:detail?id='.$dealerId);
        });
    }
}
