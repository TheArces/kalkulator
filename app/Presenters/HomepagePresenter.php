<?php

declare(strict_types=1);

namespace App\Presenters;


use App\Components\Repository\UserRepository;
use Nette\ComponentModel\IComponent;
use App\Components\Forms\CalculateForm;
use App\Components\Repository\MarginRepository;
use Nette\Diagnostics\Debugger;
use Nette\Neon\Exception;

final class HomepagePresenter extends BasePresenter
{
    private $userRepository;
    private $marginRepository;
    private $choseDealerForm;

    public function __construct(UserRepository $userRepository, CalculateForm $choseDealerForm, MarginRepository $marginRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->choseDealerForm = $choseDealerForm;
        $this->marginRepository = $marginRepository;
    }

    public function beforeRender()
    {
        $this->isLoggedIn();
        $this->template->user = $this->userRepository->getById($this->getUser()->getId());
    }
    public function renderDefault(): void
	{

	}
	public function createComponentCalculateForm(): ?IComponent
    {
        return $this->choseDealerForm->create(function ($values) {
            $toPrint = $values->inputArea;
            $margins = $this->marginRepository->getAllNotDeletedByDealerId($values->dealer_id);

            $marginBreakpoints = [];
            $marginValues = [];
            foreach ($margins as $margin){
                $marginBreakpoints[] = $margin->breakpoint;
                $marginValues[] = 1+(($margin->percent)/100);
            }

            //$toPrint = preg_replace("/[^0-9]/", "",$toPrint);

            $output = trim(preg_replace("/[^0-9]/", ' ', $toPrint));
            $exploded = explode(' ',preg_replace('!\s+!', ' ', $output));

            $left = true;
            $timesRun = -1;
            $ks = [];
            $kc = [];
            $marginUsed = [];

            try{
                foreach ($exploded as $eplod){
                    if ($left === true){
                        $timesRun++;
                        $left = false;
                        $ks[] = $eplod;
                    } else {
                        $left = true;
                        $isSet = false;
                        for ($i = count($marginValues)-1; $i >= 0; $i--) {
                            if ($ks[$timesRun] >= $marginBreakpoints[$i] && $isSet === false){
                                if ($marginValues[$i] == end($marginValues)){
                                    $kc[] = $eplod*$marginValues[$i];
                                    $marginUsed[] = $marginValues[$i];
                                } else {
                                    $bp = $marginBreakpoints[$i];
                                    $bp2 = $marginBreakpoints[$i+1];
                                    $input = $ks[$i];
                                    $bpv = max($marginValues[$i],$marginValues[$i+1]);
                                    $bpv2 = min($marginValues[$i],$marginValues[$i+1]);

                                    $diffBP = ($bp2 - $bp) / 100;
                                    $percDiff = ($input - $bp) / $diffBP;
                                    $diffBPV = ($bpv - $bpv2) /100;
                                    $BPVPD = $diffBPV * $percDiff;
                                    $bpvt = $bpv - $BPVPD;

                                    $kc[] = $eplod*$bpvt;
                                    $marginUsed[] = $bpvt;
                                }
                                $isSet = true;
                            }
                        }
                        if ($isSet === false){
                            $kc[] = $eplod*$marginValues[0];
                            $marginUsed[] = $marginValues[0];
                        }
                    }
                }
                $this->flashMessage("Vstup","print2");
                $this->flashMessage("Výstup","print");
                $this->flashMessage("Marže","print3");
                for ($i = 0; $i < count($ks); $i++) {
                    $this->flashMessage("{$ks[$i]}ks ... {$kc[$i]}kč + DPH","print");
                    $this->flashMessage("(".((($marginUsed[$i])-1)*100)."%)","print3");
                }
                $this->flashMessage("{$toPrint}","print2");
                $this->redirect('Homepage:');
            } catch (Exception $e){
                $this->AlertError('Akce se nezdařila, ujistěte se že jste zadali hodnoty ve formátu např: "100ks 100kc" ve dvojicích kusy cena', "Homepage:");
            }
        });
    }
}
