<?php

namespace App\Components\Repository;

use mysql_xdevapi\Exception;
use Nette;


class BaseRepository
{
    use Nette\SmartObject;

    private $database;
    private $tableName;

    /**
     * Sets table name to called function
     * @param String $tableName
     */
    public function setTableName($tableName){
        $this->tableName = $tableName;
    }
    public function __construct(Nette\Database\Context $database){
        $this->database = $database;
    }

    /**
     * @return Nette\Database\Context
     */
    public function getConnection(){
        return $this->database;
    }

    /**
     * Returns all rows
     * @return Nette\Database\Table\Selection
     */
    public function getAll(){
        return $this->getConnection()->table($this->tableName);
    }

    /**
     * Returns all rows not deleted (if "deleted" not found, returns all instead)
     * @return Nette\Database\Table\Selection
     */
    public function getAllNotDeleted(){
        try{
            $isDeleted = ["deleted = 0"];
            return $this->getWhere($isDeleted);
        } catch (Exception $e){
            return $this->getAll();
        }
    }

    /**
     * Returns all rows not deleted (if "deleted" not found, returns all instead)
     * @return Nette\Database\Table\Selection
     */
    public function getAllNotDeletedById($id){
        try{
            $isDeleted = ["deleted = 0", "id = ".$id];
            return $this->getWhere($isDeleted);
        } catch (Exception $e){
            return $this->getById($id);
        }
    }

    /**
     * Returns all rows with the correct id
     * @param int $id
     * @return false|Nette\Database\Table\ActiveRow
     */
    public function getById($id){
        return $this->getAll()->get($id);
    }


    /**
     * @param array $data
     * @return Nette\Database\Table\Selection
     */
    public function getWhere(array $data) : Nette\Database\Table\Selection
    {
        return $this->getAll()->where($data);
    }

    /**
     * Deletes row with the correct id
     * @param int $id
     */
    public function delete(int $id){
        $this->getById($id)->delete();
    }

    /**
     * Sets deleted row with the correct id
     * @param int $id
     */
    public function setDeleted($id){
        $data = [
            'deleted' => 1
        ];
        $this->update($id,$data);
    }

    /**
     * Inserts new row into the table
     * @param $values
     */
    public function insert($values){
        $this->getAll()->insert($values);
    }

    /**
     * Updates row with the correct id
     * @param int $id
     * @param $values
     */
    public function update($id, $values){
        $this->getById($id)->update($values);
    }

    /**
     * Inserts values if no ID provided, otherwise it updates row with the correct id
     * @param $values
     * @param int $id
     */
    public function save($values, $id){
        if ($id){
            $this->update($id,$values);
        } else {
            $this->insert($values);
        }
    }
}