<?php

namespace App\Components\Repository;

use Nette;


class DealersRepository extends BaseRepository
{
    use Nette\SmartObject;

    const TABLE = "dealers";

    public function __construct(Nette\Database\Context $database)
    {
        parent::__construct($database);
        $this->setTableName(self::TABLE);
    }

}