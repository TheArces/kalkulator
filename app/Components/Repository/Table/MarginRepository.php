<?php

namespace App\Components\Repository;

use Nette;


class MarginRepository extends BaseRepository
{
    use Nette\SmartObject;

    const TABLE = "margin";

    public function __construct(Nette\Database\Context $database)
    {
        parent::__construct($database);
        $this->setTableName(self::TABLE);
    }
    public function getAllNotDeletedByDealerId($id){
        try{
            return $this->getConnection()->query("
                SELECT * 
                FROM `margin` 
                WHERE (deleted = 0) AND (dealer_id = ?)
                ORDER BY breakpoint
                ", $id);
        } catch (Exception $e){
            return $this->getById($id);
        }
    }
}