<?php

namespace App\Components\Repository;

use Nette;


class UserRepository extends BaseRepository
{
    use Nette\SmartObject;

    const TABLE = "users";

    public function __construct(Nette\Database\Context $database)
    {
        parent::__construct($database);
        $this->setTableName(self::TABLE);
    }
}