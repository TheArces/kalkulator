<?php

declare(strict_types=1);

namespace App\Components\Forms;

use App\Components\Repository;
use Nette;
use Nette\Application\UI\Form;


final class SignUpFormFactory
{
	use Nette\SmartObject;

	private const PASSWORD_MIN_LENGTH = 7;

	/** @var FormFactory */
	private $factory;

	/** @var Repository\UserManager */
	private $userManager;


	public function __construct(FormFactory $factory, Repository\UserManager $userManager)
	{
		$this->factory = $factory;
		$this->userManager = $userManager;
	}


	public function create($editedUserId, callable $onSuccess): Form
	{
		$form = $this->factory->create();
		$form->addText('username', 'Zadejte jméno:')
			->setRequired('Zadejte jméno');

		$form->addEmail('email', 'Zadejte email:')
			->setRequired('Zadejte email');

		$form->addPassword('password', 'Zadejte heslo:')
			->setOption('description', sprintf('nejméně %d znaků', self::PASSWORD_MIN_LENGTH))
			->setRequired('Zadejte heslo.')
			->addRule($form::MIN_LENGTH, null, self::PASSWORD_MIN_LENGTH);

        $form->addText('role', "Role uživatele (1 může upravovat dodavatele, 2 i uživatele)")
            ->setHtmlType("number")
            ->setRequired('Zadejte roli.');

		$form->addSubmit('send', 'Uložit');

		$form->onSuccess[] = function (Form $form, \stdClass $values) use ($onSuccess, $editedUserId): void {
            if ($editedUserId != null){
                $this->userManager->edit($values->username, $values->email, $values->password, $editedUserId, $values->role);
            } else {
                $this->userManager->add($values->username, $values->email, $values->password, $values->role);
            }

            $onSuccess();
		};

		return $form;

	}
}
