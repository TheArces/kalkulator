<?php

namespace App\Components\Forms;

use App\Components\Repository\MarginRepository;
use Nette\Application\UI\Form;

class MarginForm
{
    private $marginRepository;
    private $factory;

    public function __construct(FormFactory $factory, MarginRepository $marginRepository)
    {
        $this->marginRepository = $marginRepository;
        $this->factory = $factory;
    }

    public function create($dealerId, callable $onSuccess) : Form
    {
        $form = $this->factory->create();
        $form->addText('breakpoint','Od kusů')->setRequired('Zadejte prosím kusy')
            ->setHtmlAttribute('class="form-control"');
        $form->addText('percent','% marže')->setRequired('Zadejte prosím marži')
            ->setHtmlAttribute('class="form-control"');
        $form->addHidden('dealer_id')->setValue($dealerId);

        $form->addSubmit('send','Uložit a publikovat')->setHtmlAttribute('class','btn btn-primary');
        $form->onSuccess[] = function (Form $form, $values) use ($onSuccess, $dealerId) {
            $this->marginRepository->insert($values);
            $onSuccess($dealerId);
        };
        return $form;
    }

}