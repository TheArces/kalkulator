<?php

namespace App\Components\Forms;

use App\Components\Repository\DealersRepository;
use Nette\Application\UI\Form;

class DealerForm
{
    private $dealersRepository;
    private $factory;

    public function __construct(FormFactory $factory, DealersRepository $dealersRepository)
    {
        $this->dealersRepository = $dealersRepository;
        $this->factory = $factory;
    }

    public function create($dealerId, callable $onSuccess) : Form
    {
        $form = $this->factory->create();
        $form->addText('name','Název')->setRequired('Zadejte prosím název')
            ->setHtmlAttribute('class="form-control"');

        $form->addSubmit('send','Uložit a publikovat')->setHtmlAttribute('class','btn btn-primary');
        $form->onSuccess[] = function (Form $form, $values) use ($onSuccess, $dealerId) {
            $this->dealersRepository->save($values, $dealerId);
            $onSuccess();
        };
        return $form;
    }

}