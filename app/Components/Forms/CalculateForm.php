<?php

namespace App\Components\Forms;

use App\Components\Repository\DealersRepository;
use Nette\Application\UI\Form;

class CalculateForm
{
    private $dealersRepository;
    private $factory;

    public function __construct(FormFactory $factory, DealersRepository $dealersRepository)
    {
        $this->dealersRepository = $dealersRepository;
        $this->factory = $factory;
    }

    public function create(callable $onSuccess) : Form
    {
        $dealersRaw = $this->dealersRepository->getAllNotDeleted();
        $dealers = [];
        foreach ($dealersRaw as $row){
            $dealers[$row->id] = $row->name;
        }

        $form = $this->factory->create();

        $form->addSelect('dealer_id','Dodavatel',$dealers)->setHtmlAttribute('class="form-control"');
        $form->addTextArea('inputArea')->setHtmlAttribute('class="form-control" rows="10"');

        $form->addSubmit('send','Spočítat')->setHtmlAttribute('class','btn btn-primary');
        $form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
            $onSuccess($values);
        };
        return $form;
    }

}